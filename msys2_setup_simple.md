# MSYS2 Setup

## Prerequisites

----------------------------------

1. Running a 64-bit version of Windows 10 or Windows 11 that is up to date.
2. Basic knowledge of Windows environment.

## Why use MSYS2?

----------------------------------

1. Beginner friendly development environment
2. Can build native Win32 or Win64 executables
3. A "linux" like environment, since that is my comfort zone

## Installation of Optional applications

----------------------------------
{Have to be in powershell as admin}

* Microsoft Windows Terminal (Default on Windows 11) {recommended} \
    `winget install --id Microsoft.WindowsTerminal`

* Microsoft Visual Studio Code <https://code.visualstudio.com> {recommended} \
    {Use the Windows x64 System Installer} \
    `winget install --id Microsoft.VisualStudioCode --scope machine -i`

## Installation of MSYS2

----------------------------------
{Have to be in powershell as admin}

* MSYS2 \
    `winget install --id MSYS2.MSYS2 -i`

## Configure Windows Terminal {optional}

----------------------------------

```js
{
    //"backgroundImage": "C:/Users/%USERNAME%/Pictures/wallpaper/linux_matrix_wallpaper.jpg",
    "closeOnExit": "always",
    "commandline": "C:/msys64/msys2_shell.cmd -defterm -here -no-start -mingw64",
    "guid": "{17da3cac-b318-431e-8a3e-7fcdefe6d114}",
    "hidden": false,
    "icon": "C:/msys64/mingw64.ico",
    "name": "MINGW64",
    "startingDirectory": "C:/msys64/home/%USERNAME%",
    "tabTitle": "MINGW64"
}
```

## Setting up MSYS2

----------------------------------

1. Now to check for updates and add packages needed for build toolchain in MSYS2 \
    {Add ILoveCandy to /etc/pacman.conf}

    a. Update system critical packages \
        `pacman -Syu` \
        {Might have to close all running processes after update}

    b. Update any remaining packages \
        `pacman -Su`
2. Install common extraction tools, misc. tools, and base development tools \
    `pacman -S --needed p7zip unrar unzip zip vim dos2unix git \` \
    `man-db man-pages-posix pkgfile base-devel msys2-devel`
3. Install initial development environment gcc \
    `pacman -S --needed mingw-w64-x86_64-{toolchain,autotools,cmake,nasm,meson}`
4. Update pkgfile database \
    `pkgfile --update`
5. Install pdjlib (Used early on for getting and validating input)

    ```bash
        mkdir -pv ~/projects
        git clone https://gitlab.com/pdjprogrammer/pdjlib.git ~/projects/pdjlib
        cd ~/projects/pdjlib
        make
        make install
    ```

## Configure VS Code {Optional}

----------------------------------

1. Create a special shortcut for VSCode \
    `C:\dev\vscode\Code.exe --extensions-dir "D:\vscode_msys2" --user-data-dir "D:\vscode_msys2"`
2. Add some custom settings to VSCode {Any line with // is optional}

    ```js
    {
        "editor.insertSpaces": true,
        "editor.tabSize": 4,
        //"editor.colorDecorators": false,
        //"editor.fontFamily": "FiraCode Nerd Font",
        //"editor.fontSize": 16,
        //"editor.renderWhitespace": "boundary",
        //"editor.wordWrap": "off",

        //"extensions.ignoreRecommendations": true,

        //"files.associations": {
            //"*.json": "jsonc",
            //"*.h": "c",
            //"*.hpp": "cpp"
        //},

        "files.eol": "\n",
        //"files.hotExit": "off",
        "files.insertFinalNewline": true,
        "files.trimFinalNewlines": true,
        "files.trimTrailingWhitespace": true,

        //"security.workspace.trust.banner": "never",
        //"security.workspace.trust.emptyWindow": false,
        //"security.workspace.trust.enabled": false,
        //"security.workspace.trust.startupPrompt": "never",

        //"telemetry.telemetryLevel": "error",

        "terminal.integrated.profiles.windows": {
            "Powershell": null,
            "Command Prompt": null,
            "Git Bash": null,
            "MINGW64": {
                "path": "C:/msys64/usr/bin/bash.exe",
                "args": [
                    "--login",
                    "-i"
                ],
                "env": {
                    "MSYSTEM": "MINGW64",
                    "CHERE_INVOKING": "1",
                    "MSYS2_PATH_TYPE": "inherit"
                }
            }
        },

        "terminal.integrated.defaultProfile.windows": "MINGW64",

        //"window.restoreWindows": "none",

        //"workbench.startupEditor": "none",

        "[makefile]": {
            "editor.insertSpaces": false
        }
    }
    ```

3. Add some extensions to VSCode
    In Extensions Search field enter `ms-vscode.cpptools fougas.msys2` \

## Demonstration of our new build environment in use

----------------------------------

1. Simple CLI application
    a. Create a new project folder {hello} \
    `mkdir -pv ~/projects/hello`
    b. Create hello.c

    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <pdjlib.h>

    int main(void)
    {
        string name = get_string("Enter your first name: ");
        int randomNumber = get_int("Enter a random whole number: ");

        printf("Hello, %s!\n", name);
        printf("%d is the number you entered.\n", randomNumber);

        return EXIT_SUCCESS;
    }
    ```

    c. Open a terminal \
    `gcc hello.c -lpdjlib -o hello`
    d. Run the compiled program \
    `./hello.exe`
